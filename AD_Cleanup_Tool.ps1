# A script that will loop through AD attributes to verify, or add information
#
# By: Landon Lengyel
# Current version: 2019-09-27



# Allow users to pass OU distinguished names through a pipeline
Param (
    [Parameter(
        ValueFromPipeline = $true
    )]
    [String]$OrganizationalUnit = $null
)

# Menu specific to AD user properties
function Users-Menu($OrganizationalUnit)
{
    # Loop until a valid option is picked (stop typos)
    Do {
        clear

        # Show script user the different options
        Write-Host "=== Type the Property you wish to verify ==="
        Write-Host
        Write-Host "* Company"
        #Write-Host "* Country" # country is currently not working
        Write-Host "* Department"
        Write-Host "* Description"
        Write-Host "* Display Name"
        Write-Host "* Job Title"
        Write-Host "* Manager"
        Write-Host "* Office"
        Write-Host "* Mobile Phone Number"
        Write-Host "* State/Province"
        Write-Host "* Telephone Number"
        #Write-Host "* Computer object/CMDB consolidation"
        Write-Host "* QUIT"
        Write-Host

        # Assume valid option, change when user hits default switch
        $ValidOption = $True

        $UserOption = Read-Host "Option"
        clear

        # Use provided option
        switch($UserOption){
            'company'{ Users-Company($OrganizationalUnit) }
            'country'{ Users-Country($OrganizationalUnit) }
            'department'{ Users-Department($OrganizationalUnit) }
            'description'{ Users-Description($OrganizationalUnit) }
            'display name'{ Users-Display_Name($OrganizationalUnit) }
            'job title'{ Users-Job_Title($OrganizationalUnit) }
            'manager'{ Users-Manager($OrganizationalUnit) }
            'office'{ Users-Office($OrganizationalUnit) }
            'mobile phone number'{ Users-Mobile_Phone_Number($OrganizationalUnit) }
            'state/province'{ Users-State_Province($OrganizationalUnit) }
            'state'{ Users-State_Province($OrganizationalUnit) }
            'telephone number'{ Users-Telephone_Number($OrganizationalUnit) }
            'computer vision remediation' { Vision-AD-Consolidation($OrganizationalUnit) }
            'quit'{ exit }
            default{ $ValidOption = $False }
        }

    } Until ($ValidOption -eq $True)
}


# Menu specific to AD Security Group properties
function Security-Group-Menu($OrganizationalUnit){

    # Loop until a valid option is picked (stop typos)
    Do {
        $ValidOption = $True
        
        clear

        # Write all available options
        Write-Host
        Write-Host
        Write-Host "=== Type the Property you wish to verify ==="
        Write-Host
        Write-Host "* Description"
        Write-Host "* QUIT"
        Write-Host

        $UserOption = Read-Host "Option"
        clear

        # Use provided options
        switch($UserOption){
            'description'{ Security-Groups-Description($OrganizationalUnit) }
            'quit'{ exit }
            default { $ValidOption = $False }
        }

    } Until ($ValidOption -eq $True)
}


function Gather-User-Data
{
    Write-Host "Gathering data, please wait..."
    try { 
        $users = Get-ADUser -Filter * -SearchBase $OrganizationalUnit -Properties * 
        return $users
    }
    catch { 
        Write-Host "An error occurred, and the script must exit"
        #Write-Host "$Error"
        exit
    }
}


# checks for automated entry
# returns either $false (no automated entry) or the value the user would like to use for automated entry
function Check-For-Auto
{
    # loop until the user returns an accepted value
    do {
        Write-Host
        Write-Host
        Write-Host
        Write-Host
        Write-Host -NoNewline 'Would you like to enable '; Write-Host -NoNewLine -ForegroundColor Yellow 'automated entry '; Write-Host 'for this OU?'
        $status = Read-Host 'Yes/No/Help'

        if (($status -eq 'yes') -or ($status -eq 'y')){
            $defaultValue = Read-Host "What value should we use?"
            return $defaultValue
        }
        elseif ($status -eq 'help') {
            clear
            # one line with selectively colored text
            Write-Host -NoNewLine 'Automated entry allows for adding '; Write-Host -NoNewline -Foreground Yellow 'one attribute '; Write-Host -NoNewLine 'to '; Write-Host -Foreground Yellow 'multiple users'
            Write-Host "For example: All users in a specified OU work in the State `'Utah`' - Rather than going through every user and adding `'Utah`' to their profile, this script will loop through all users and add `'Utah`'"
            Write-Host
            Read-Host 'Presss enter to continue'
        }
        else {
            return $false
        }

    } while ($status -eq 'help')
}



########## Functions for cleaning up different user properties ##########

function Users-Company ($OrganizationalUnit)
{
    $users = Gather-User-Data

    $autoValue = Check-For-Auto


    if ($autoValue -ne $false ){
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.company -ne $autoValue){
                Write-Host ("## " + $user.name + " is missing correct company")

                Set-ADUser -Identity $user.objectGUID -Company $autoValue
            }
        }
    }
    else {
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.company -eq $null){
                $correction = Read-Host ("## " + $user.name + " is missing company")

                if ($correction -eq $null) { Write-Host "Ignored..." }
                else { Set-ADUser -Identity $user.objectGUID -Company $correction }
            }
        }
    }
}
function Users-Country ($OrganizationalUnit)
{`
    $users = Gather-User-Data

    Write-Host "!!! Currently, only the United States (with auto mode) is supported for countries. Sorry for any inconvenience this may cause"

    $continue = Read-Host "Should the program continue in auto mode? (Yes/No)"
    if ($continue -eq "yes" -or $continue -eq "y") { <# just continue #> }
    else { exit }
    

    ForEach($user in $users){
        Write-Host ("Checking: " + $user.name)
        if ($user.country -ne "US"){
            Write-Host ("## " + $user.name + " is missing country")

            Set-ADUser -Identity $user.objectGUID -country "US"
        }
        if ($user.co -ne "United States"){
            Set-ADUser -Identity $user.objectGUID -co "United States"
        }
        if ($user.countryCode -ne 840){
            Set-ADUser -Identity $user.objectGUID -countryCode 840
        }
    }
}
function Users-Department ($OrganizationalUnit)
{
    $users = Gather-User-Data

    $autoValue = Check-For-Auto

    if ($autoValue -ne $false){
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.department -ne $autoValue){
                Write-Host ("## " + $user.name + " is missing correct department")
                
                Set-ADUser -Identity $user.objectGUID -department $autoValue
            }
        }
    }
    else{ 
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.department -eq $null){
                $correction = Read-Host ("## " + $user.name + " is missing department")

                if ($correction -eq $null) { Write-Host "Ignored..." }
                else { Set-ADUser -Identity $user.objectGUID -department $correction }
            }
        }
    }
}
function Users-Description ($OrganizationalUnit)
{
    $users = Gather-User-Data

    $autoValue = Check-For-Auto

    if ($autoValue -ne $false){
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.description -ne $autoValue){
                Write-Host ("## " + $user.name + " is missing correct description")
                
                Set-ADUser -Identity $user.objectGUID -description $autoValue
            }
        }
    }
    else {
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.description -eq $null){
                $correction = Read-Host ("## " + $user.name + " is missing description")

                if ($correction -eq $null) { Write-Host "Ignored..." }
                else { Set-ADUser -Identity $user.objectGUID -description $correction }
            }
        }
    }
}
function Users-Display_Name ($OrganizationalUnit)
{
    $users = Gather-User-Data

    ForEach($user in $users){
        Write-Host ("Checking: " + $user.name)
        if ($user.displayName -eq $null){
            $correction = Read-Host ("## " + $user.name + " is missing display name")

            if ($correction -eq $null) { Write-Host "Ignored..." }
            else { Set-ADUser -Identity $user.objectGUID -displayName $correction }
        }
    }
}
function Users-Job_Title ($OrganizationalUnit)
{
    $users = Gather-User-Data

    $autoValue = Check-For-Auto

    if ($autoValue -ne $false){
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.title -ne $autoValue){
                Write-Host ("## " + $user.name + " is missing correct job title")
                
                Set-ADUser -Identity $user.objectGUID -title $autoValue
            }
        }
    }
    else {
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.title -eq $null){
                $correction = Read-Host ("## " + $user.name + " is missing job title")

                if ($correction -eq $null) { Write-Host "Ignored..." }
                else { Set-ADUser -Identity $user.objectGUID -title $correction }
            }
        }
    }
}
function Users-Manager ($OrganizationalUnit)
{
    $users = Gather-User-Data

    $autoValue = Check-For-Auto

    if ($autoValue -ne $false){
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.manager -ne $autoValue){
                Write-Host ("## " + $user.name + " is missing correct manager")

                Set-ADUser -Identity $user.objectGUID -manager $autoValue
            }
        }
    }
    else{
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)

            # ensure inputed manager is a user
            if ($user.manager -eq $null){
                Do {

                    try {
                        $managerUserFail = $false

                        $correction = Read-Host ("## " + $user.name + " is missing manager")

                        if ($correction -eq $null) { Write-Host "No value entered - Moving on..." }
                        else { Set-ADUser -Identity $user.objectGUID -manager $correction }
                    }
                    catch { 
                        Write-Host
                        Write-Host -ForegroundColor Red 'Manager field must be a user located in same domain controller'
                        $managerUserFail = $true
                    }

                } While ($managerUserFail -eq $true)
            }
        } # complete individual user loop
    }
}
function Users-Office ($OrganizationalUnit)
{
    $users = Gather-User-Data

    $autoValue = Check-For-Auto

    if ($autoValue -ne $false){
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.physicalDeliveryOfficeName -ne $autoValue){
                Write-Host ("## " + $user.name + " is missing correct office name")
                
                Set-ADUser -Identity $user.objectGUID -Office $autoValue
            }
        }
    }
    else {
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.physicalDeliveryOfficeName -eq $null){
                $correction = Read-Host ("## " + $user.name + " is missing office")

                if ($correction -eq $null) { Write-Host "Ignored..." }
                else { Set-ADUser -Identity $user.objectGUID -Office $correction }
            }
        }
    }
}
function Users-Mobile_Phone_Number ($OrganizationalUnit)
{
    $users = Gather-User-Data

    ForEach($user in $users){
        Write-Host ("Checking: " + $user.name)
        if ($user.mobile -eq $null){
            $correction = Read-Host ("## " + $user.name + " is missing mobile phone number")

            if ($correction -eq $null) { Write-Host "Ignored..." }
            else { Set-ADUser -Identity $user.objectGUID -MobilePhone $correction }
        }
    }
}
function Users-State_Province ($OrganizationalUnit)
{
    $users = Gather-User-Data

    $autoValue = Check-For-Auto

    if ($autoValue -ne $false){
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.State -ne $autoValue){
                Write-Host ("## " + $user.name + " is missing correct state/province")
                
                Set-ADUser -Identity $user.objectGUID -State $autoValue
            }
        }
    }
    else {
        ForEach($user in $users){
            Write-Host ("Checking: " + $user.name)
            if ($user.State -eq $null){
                $correction = Read-Host ("## " + $user.name + " is missing state/province")

                if ($correction -eq $null) { Write-Host "Ignored..." }
                else { Set-ADUser -Identity $user.objectGUID -State $correction }
            }
        }
    }
}
function Users-Telephone_Number ($OrganizationalUnit)
{
    $users = Gather-User-Data

    ForEach($user in $users){
        Write-Host ("Checking: " + $user.name)
        if ($user.telephoneNumber -eq $null){
            $correction = Read-Host ("## " + $user.name + " is missing telephone number")

            if ($correction -eq $null) { Write-Host "Ignored..." }
            else { Set-ADUser -Identity $user.objectGUID -OfficePhone $correction }
        }
    }
}


########## Functions for cleaning up AD Security Groups ##########

function Security-Groups-Description($OrganizationalUnit){
    clear

    # Gather all security groups in the current domain, and that the user has access to
    $AllGroups = Get-ADGroup -SearchBase $OrganizationalUnit -Properties Name, Description, SID -Filter {GroupCategory -eq 'Security'} | Sort-Object Name

    # Look for groups without a description
    ForEach ($Group in $AllGroups) {

        If ($Group.Description -eq $NULL) {
            Do {

                Write-Host -ForegroundColor Yellow "`n`n`nGroup missing description: $Group.DistinguishedName"

                # Prompt user for new description
                $NewDescription = Read-Host "Enter description for group"

                # Confirm that the user is happy with the description
                Write-Host "Would you like to add the following description to the group? (y/n/ s=skip group)"
                Write-Host -ForegroundColor Cyan $NewDescription
                $UserConfirmation = Read-Host

                # If confirmed, apply the description to the group
                if ($UserConfirmation -eq "y" -or $UserConfirmation -eq "yes") {
                    Set-ADGroup -Identity $Group.SID -Description $NewDescription
                    Write-Host "Description applied`n`n"

                    break
                }
                # If skipping this group, break from loop
                ElseIf ($UserConfirmation -eq "s" -or $UserConfirmation -eq "skip") { 
                    Write-Host "Skipping group..."
                    break 
                }
                else { Write-Host "User confirmation failed"; continue} 

            } While ($True)
        }
        Else {
            Write-Host -ForegroundColor Green "Group description present: $Group.DistinguishedName"
        }
    }

    
}



############################################ "Main" ############################################


Import-Module activeDirectory

Write-Host "--- Welcome to the EAW Active Directory Cleanup Tool ---"



# Retrieve the OU to check (if not passed in a parameter or pipe)
if ($OrganizationalUnit -eq '' -or $OrganizationUnit -eq $null){
    $OrganizationalUnit = Read-Host "Please enter the DISTINGUISHED NAME of the OU you wish to check (this can be found in the OU's Attribute Editor)"
}



# Menus work in a cascading fashion
# As of writing, we have two types of verification that are seperate from each other
# User properties, and security group properties
# 
# Since these are completely seperate, it will prompt the user for whether this is concerning User properties, or Security group properties
#
# |User properties
# |
# |--Company
# |--Department
# |--Display Name
# |--etc



$ValidOption = $False
Do {
    # Check whether this is a user, or security group check
    clear
    Write-Host "=== Which type of item will be cleaned up? ==="
    Write-Host
    Write-Host "* User properties"
    Write-Host "* Security group properties"
    Write-Host "* QUIT"
    Write-Host

    $UserOption = Read-Host "Option"

    # Set to true, will change to false with a typo
    $ValidOption = $True

    switch($UserOption){
        'user properties'{ Users-Menu($OrganizationalUnit) }
        'user'{ Users-Menu($OrganizationalUnit) }
        'security group properties'{ Security-Group-Menu($OrganizationalUnit) }
        'security group'{ Security-Group-Menu($OrganizationalUnit) }
        'quit'{ exit }

        default{ $ValidOption = $False }
    }
} Until ($ValidOption -eq $True)


Read-Host "Press enter to exit"


# Test: OU=Utah,DC=eawphx,DC=edatwork,DC=com
